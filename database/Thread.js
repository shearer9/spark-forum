const db = require('./db')
const {check, validationResult, param, query} = require('express-validator')

let threads = {}

// Validacija input polja,query parametara itd.
threads.validate = (method) => {
    switch (method) {
        case 'createThread': {
            return [
                check('title', `Title doesn't exist`).not().isEmpty(),
                check('body', `Body can't be empty`).not().isEmpty()
            ]
        }
        case 'editThread': {
            return [
                check('body', `Body can't be empty`).not().isEmpty(),
                param('id', `Wrong URL`).isInt()
            ]
        }
        case 'deleteThread': {
            return [
                param('id', `Wrong URL`).isInt()
            ]
        }
        case 'getThread': {
            return [
                param('id', `Wrong URL`).isInt()
            ]
        }
        case 'getThreadsWithPagination': {
            return [
                query('page', `Invalid query parameters.`).optional().isInt(),
                query('perPage', `Invalid query parameters.`).optional().isInt()
            ]
        }
        case 'searchThreads': {
            return [
                check('searchText', `Search text field can't be empty`).not().isEmpty()
            ]
        }
    }
}

// Spremanje error poruka u jedan objekt i vracanje responsea ako ih ima
// threads.checkErrors = (req, res) => {
//     let errors = validationResult(req).errors.map(item => item.msg)
//     if (errors.length > 0) {
//         res.status(422).json({errors: errors});
//     }
// }

// Dohvacanje teme sa odredenim IDom i vracanje responsea
threads.get = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    threads.findThreadById(req.params.id).then(thread => {
        if (!thread) return res.status(404).json({error: 'Ova tema ne postoji'})
        return res.json(thread)
    })
}

// Dohvacanje svih tema ili ako se proslijede page i perPage parametri onda cemo imat paginaciju
threads.getAll = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    const page = parseInt(req.query.page)
    const perPage = parseInt(req.query.perPage)
    let sql = page && perPage ? 'SELECT * FROM THREADS LIMIT ?, ?' : 'SELECT * FROM THREADS';
    const pageNumber = (page - 1) * perPage
    db.query(sql, [pageNumber, perPage], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je', error: err})
        } else {
            const plainObjectResult = JSON.parse(JSON.stringify(result))
            return res.json(plainObjectResult)
        }
    })
}

// Trazenje tema sa odredenim titleom
threads.searchThreads = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    let searchText = req.body.searchText
    searchText = '%' + searchText + '%';
    let sql = 'SELECT * FROM threads WHERE title LIKE ?'
    db.query(sql, [searchText], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je', error: err})
        } else {
            const plainObjectResult = JSON.parse(JSON.stringify(result))
            return res.json(plainObjectResult)
        }
    })
}

// Trazenje tema koje imaju komentare koji se sastoje od traženog teksta.
threads.searchThreadsByComments = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    let searchText = req.body.searchText
    searchText = '%' + searchText + '%';
    let sql = 'SELECT * FROM threads WHERE id IN ( SELECT DISTINCT(thread_id) FROM comments WHERE comments.body LIKE ?)'
    db.query(sql, [searchText], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je', error: err})
        } else {
            const plainObjectResult = JSON.parse(JSON.stringify(result))
            return res.json(plainObjectResult)
        }
    })
}

// Dohvacanje teme sa njezinim komentarima
threads.getWithComments = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    threads.findThreadById(req.params.id).then(thread => {
        if (!thread) return res.status(404).json({error: 'Ova tema ne postoji'})
        threads.threadComments(req.params.id).then(result => {
            thread.comments = result
            return res.json(thread)
        })
    })
}

// Dohvacanje komentara za neku odredenu temu (PROMISE)
threads.threadComments = (id) => {
    let sql = `SELECT * FROM comments WHERE thread_id = ? ORDER BY created_at DESC`
    return new Promise((resolve, reject) => {
        db.query(sql, [id], (err, result) => {
            if (err) {
                return reject(err)
            }
            if (result.length) {
                const plainObjectResult = JSON.parse(JSON.stringify(result))
                return resolve(plainObjectResult)
            }
            return resolve(result.length);
        })
    })
}

// Dohvacanje teme sa odredenim IDom (PROMISE)
threads.findThreadById = (id) => {
    let sql = `SELECT * FROM threads WHERE id = ?`
    return new Promise((resolve, reject) => {
        db.query(sql, [id], (err, result) => {
            if (err) {
                return reject(err)
            }
            if (result.length) {
                const plainObjectResult = JSON.parse(JSON.stringify(result[0]))
                return resolve(plainObjectResult)
            }
            return resolve(result.length);
        })
    })
}

// Kreiranje teme
threads.create = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    let sql = `INSERT INTO threads (author_id,title,body) VALUES (?,?,?)`
    const {title, body} = req.body
    const author_id = req.user.id
    db.query(sql, [author_id, title, body], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je', error: err})
        }
        if (result.affectedRows > 0) {
            return res.json({message: 'Uspješno ste napravili novu temu.'})
        }
    })
}

// Uređivanje teme
threads.edit = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    const {body} = req.body
    const userId = req.user.id
    const threadId = req.params.id
    threads.findThreadById(threadId).then(thread => {
        if (thread.author_id !== userId) {
            return res.status(422).json({error: 'Možete uređivati samo vlastite teme'})
        }
    }).catch(error => res.status(400).json(error))

    let sql = `UPDATE threads SET body = ? WHERE id = ?;`
    db.query(sql, [body, threadId], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je', error: err})
        }
        if (result.affectedRows > 0) {
            return res.json({message: 'Uspješno ste uredili temu.'})
        }
    })
}

// Brisanje teme
threads.delete = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    const userId = req.user.id
    const threadId = req.params.id
    threads.findThreadById(threadId).then(thread => {
        if (thread.author_id !== userId) {
            return res.status(422).json({error: 'Možete brisati samo vlastite teme'})
        }
    }).catch(error => res.status(400).json(error))
    let sql = `DELETE FROM threads WHERE id = ?`
    db.query(sql, [threadId], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je', error: err})
        }
        if (result.affectedRows > 0) {
            return res.json({message: 'Uspješno ste izbrisali temu.'})
        }
    })
}

module.exports = threads