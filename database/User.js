const db = require('./db')
const {check, param, validationResult} = require('express-validator')
const crypto = require('crypto')
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer')

let users = {}

users.validate = (method) => {
    switch (method) {
        case 'createUser': {
            return [
                check('name', 'Name doesnt exists').not().isEmpty(),
                check('last_name', 'Last name doesnt exists').not().isEmpty(),
                check('username', 'Username doesnt exists').not().isEmpty().custom(value => {
                    return users.findUserByUsername(value).then(user => {
                        if (user) {
                            return Promise.reject('Username already in use');
                        }
                    });
                }),
                check('email', 'Invalid email').exists().isEmail().custom(value => {
                    return users.findUserByEmail(value).then(user => {
                        if (user) {
                            return Promise.reject('E-mail already in use');
                        }
                    });
                }),
                check('password', `Invalid password. Minimum length 8, at least one uppercase, one lowercase and digit.`).not().isEmpty().matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, "i").isLength({min: 8}),
                check('passwordConfirmation', `Password doesn't match`).custom((value, {req}) => (value === req.body.password)),
                check('ToS', 'You must accept ToS').equals('accepted')
            ]
        }
        case 'setToken': {
            return [
                check('email', 'Unijeli ste neispravan email').isEmail()
            ]
        }
        case 'resetPassword': {
            return [
                param('token', `Invalid token`).not().isEmpty(),
                check('password', `Invalid password. Minimum length 8, at least one uppercase, one lowercase and digit.`).not().isEmpty().matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, "i").isLength({min: 8}),
                check('passwordConfirmation', `Password doesn't match`).custom((value, {req}) => (value === req.body.password))
            ]
        }
    }
}

users.findUserByEmail = (value) => {
    let sql = `SELECT * FROM users WHERE email = ?`
    return new Promise((resolve, reject) => {
        db.query(sql, [value], (err, result) => {
            if (err) {
                return reject(err)
            }
            if (result.length) {
                const plainObjectResult = JSON.parse(JSON.stringify(result[0]))
                return resolve(plainObjectResult)
            }
            return resolve(result.length);
        })
    })
}

users.findUserByUsername = (value) => {
    let sql = `SELECT * FROM users WHERE username = ?`
    return new Promise((resolve, reject) => {
        db.query(sql, [value], (err, result) => {
            if (err) {
                return reject(err)
            }
            if (result.length) {
                const plainObjectResult = JSON.parse(JSON.stringify(result[0]))
                return resolve(plainObjectResult)
            }
            return resolve(result.length);
        })
    })
}

users.createUser = async (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    let sql = `INSERT INTO users (name,last_name,username,email,password) VALUES (?,?,?,?,?)`
    const {name, last_name, username, email, password} = req.body
    let hash = await bcrypt.hash(password, 10)
    db.query(sql, [name, last_name, username, email, hash], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je', error: err})
        }
        if (result.affectedRows > 0) {
            return res.json({message: 'Uspješno ste se registrirali. Možete se prijaviti.', redirect: '/auth/login'})
        }
    })
}

users.findId = (id) => {
    let sql = `SELECT * FROM users WHERE id = ?`
    return new Promise((resolve, reject) => {
        db.query(sql, [id], (err, result) => {
            if (err) {
                return reject(err)
            }
            if (result.length) {
                const plainObjectResult = JSON.parse(JSON.stringify(result[0]))
                return resolve(plainObjectResult)
            }
            return resolve(result.length);
        })
    })
}

users.setToken = (email, token) => {
    let sql = `UPDATE users SET reset_password_token = ?, reset_token_expires = ADDTIME(now(), '01:00:00') WHERE email = ?`
    return new Promise((resolve, reject) => {
        db.query(sql, [token, email], (err, result) => {
            if (err) {
                return reject(err)
            }
            if (result.length) {
                const plainObjectResult = JSON.parse(JSON.stringify(result))
                return resolve(plainObjectResult)
            }
            return resolve(result.length);
        })
    })
}

users.resetPassword = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    users.findUserByEmail(req.body.email).then(async user => {
        if (!user) return res.status(400).json({error: 'Ne postoji taj email u bazi'})
        const token = await crypto.randomBytes(20).toString('hex')
        try {
            await users.setToken(user.email, token)
        } catch (err) {
            return res.status(400).json({error: 400})
        }
        users.createMail(req.headers.host, token, user.email, res)
    })
}

users.setNewPassword = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    const password = req.body.password
    users.findUserByToken(req.params.token).then(async user => {
        if (!user) return res.status(400).json({error: 'Ne postoji taj token u bazi ili mu je isteklo vrijeme trajanja.'})
        const sql = 'UPDATE users SET password = ? WHERE id = ?'
        const hash = await bcrypt.hash(password, 10)
        db.query(sql, [hash, user.id], (err, result) => {
            if (err) {
                return res.status(422).json({message: 'Žao nam je', error: err})
            }
            if (result.affectedRows > 0) {
                return res.json({
                    message: 'Uspješno ste postavili novu lozinku. Možete se prijaviti.',
                    redirect: '/auth/login'
                })
            }
        })
    })
}

users.createMail = (host, token, email, res) => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.MAIL_CONFIG_USER,
            pass: process.env.MAIL_CONFIG_PASSWORD
        }
    });

    const mailOptions = {
        from: process.env.MAIL_CONFIG_USER,
        to: email,
        subject: 'Sending Email using Node.js',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
            'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
            'http://' + host + '/reset_password/' + token + '\n\n' +
            'If you did not request this, please ignore this email and your password'
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return res.status(400).json({status: 422, message: "Nismo Vam mogli poslat mail."});
        } else {
            return res.json({message: 'Provjerite vaš email.'})
        }
    });
}

users.findUserByToken = (token) => {
    let sql = `SELECT * FROM users WHERE reset_password_token = ? AND reset_token_expires > NOW()`
    return new Promise((resolve, reject) => {
        db.query(sql, [token], (err, result) => {
            if (err) {
                return reject(err)
            }
            if (result.length) {
                const plainObjectResult = JSON.parse(JSON.stringify(result[0]))
                return resolve(plainObjectResult)
            }
            return resolve(result.length);
        })
    })
}

module.exports = users