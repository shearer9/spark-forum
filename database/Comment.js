const db = require('./db')
const {check, validationResult, param} = require('express-validator')

let comments = {}

comments.validate = (method) => {
    switch (method) {
        case 'createComment': {
            return [
                check('body', `Body can't be empty`).not().isEmpty(),
                param('id', `Wrong URL`).isInt()
            ]
        }
        case 'editComment': {
            return [
                check('body', `Body can't be empty`).not().isEmpty(),
                param('id', `Wrong URL`).isInt()
            ]
        }
        case 'deleteComment': {
            return [
                param('id', `Wrong URL`).isInt()
            ]
        }
        case 'searchComments': {
            return [
                check('searchText', `Search text field can't be empty`).not().isEmpty()
            ]
        }
    }
}

// comments.checkErrors = (req, res) => {
//     let errors = validationResult(req).errors.map(item => item.msg)
//     if (errors.length > 0) {
//         return res.status(422).json({errors: errors});
//     }
// }

// Dohvaćanje komentara sa odredenim IDom (PROMISE)
comments.findCommentById = (id) => {
    let sql = `SELECT * FROM comments WHERE id = ?`
    return new Promise((resolve, reject) => {
        db.query(sql, [id], (err, result) => {
            if (err) {
                return reject(err)
            }
            if (result.length) {
                const plainObjectResult = JSON.parse(JSON.stringify(result[0]))
                return resolve(plainObjectResult)
            }
            return resolve(result.length);
        })
    })
}

// Trazenje komentara sa odredenim body-em
comments.searchComments = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    let searchText = req.body.searchText
    searchText = '%' + searchText + '%';
    let sql = 'SELECT * FROM comments WHERE body LIKE ?'
    db.query(sql, [searchText], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je', error: err})
        } else {
            const plainObjectResult = JSON.parse(JSON.stringify(result))
            return res.json(plainObjectResult)
        }
    })
}

// Dodavanje novog komentara
comments.create = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    let sql = `INSERT INTO comments (body,user_id,thread_id) VALUES (?,?,?)`
    const {body} = req.body
    const userId = req.user.id
    const threadId = req.params.id
    db.query(sql, [body, userId, threadId], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je.', error: err})
        }
        if (result.affectedRows > 0) {
            return res.json({message: 'Uspješno ste dodali komentar.'})
        }
    })
}

// Uređivanje komentara
comments.edit = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    const {body} = req.body
    const userId = req.user.id
    const commentId = req.params.id

    comments.findCommentById(commentId).then(comment => {
        if (comment.user_id !== userId) {
            console.log(comment.user_id);
            return res.status(422).json({error: 'Možete uređivati samo vlastite komentare.'})
        }
    }).catch(error => res.status(400).json(error))
    let sql = `UPDATE comments SET body = ? WHERE id = ?;`
    db.query(sql, [body, commentId], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je', error: err})
        }
        if (result.affectedRows > 0) {
            return res.json({message: 'Uspješno ste uredili komentar.'})
        }
    })
}

// Brisanje komentara
comments.delete = (req, res) => {
    let errors = validationResult(req).errors.map(item => item.msg)
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    }
    const userId = req.user.id
    const commentId = req.params.id
    comments.findCommentById(commentId).then(comment => {
        if (comment.user_id !== userId) {
            return res.status(422).json({error: 'Možete brisati samo vlastite komentare'})
        }
    }).catch(error => res.status(400).json(error))
    let sql = `DELETE FROM comments WHERE id = ?`
    db.query(sql, [commentId], (err, result) => {
        if (err) {
            return res.status(422).json({message: 'Žao nam je', error: err})
        }
        if (result.affectedRows > 0) {
            return res.json({message: 'Uspješno ste izbrisali komentar.'})
        }
    })
}

module.exports = comments