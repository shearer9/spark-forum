const express = require('express');
const bodyParser = require('body-parser');
const config = require('dotenv').config();
const passport = require('passport');
require('./passport');

const port = process.env.PORT || '3000'
const app = express();
const router = express.Router();
const auth = require('./routes/auth');

const userRoutes = require('./routes/users')
const threadRoutes = require('./routes/threads')
const commentRoutes = require('./routes/comments')

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/auth', auth);
app.use('/comments', passport.authenticate('jwt', {session: false}), commentRoutes);
app.use('/threads', passport.authenticate('jwt', {session: false}), threadRoutes);
app.use('/user', userRoutes);

app.use((req, res) => {
    res.status(404).json({status: 404, title: "Not Found"});
});

app.listen(port, () => {
    console.log('Server started');
})

