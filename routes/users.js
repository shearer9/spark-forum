const express = require('express')
const router = express.Router()
const User = require('../database/User')

// Šalje se email polje
router.post(
    '/reset_password',
    User.validate('setToken'),
    User.resetPassword
)

// Salje se password i passwordConfirmation polja
router.post(
    '/reset_password/:token',
    User.validate('resetPassword'),
    User.setNewPassword
)

module.exports = router