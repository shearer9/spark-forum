const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const User = require('../database/User')


/* POST login. */

// Slati email i password polja
router.post('/login', function (req, res) {
    passport.authenticate('local', {session: false}, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: info ? info.message : 'Login failed',
                user: user,
                err: err
            });
        }
        req.login(user, {session: false}, (err) => {
            if (err) {
                res.send(err);
            }
            const userId = {id: user.id, username: user.username, email: user.email}
            const token = jwt.sign(userId, process.env.JWT_SECRET_KEY, {expiresIn: '1h'});
            return res.json({userId, token});
        });
    })(req, res);
});

// Slati polja :
// name,
// last_name,
// username,
// password,
// passwordConfirmation,
// ToS
router.post(
    '/register',
    User.validate('createUser'),
    User.createUser
)

module.exports = router;
