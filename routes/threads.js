const express = require('express')
const router = express.Router()
const Thread = require('../database/Thread')
const Comment = require('../database/Comment')

// ako se proslijedi page i perPage query parametri onda ce bit paginacije, inace vraca sve threadove
router.get('/',
    Thread.validate('getThreadsWithPagination'),
    Thread.getAll
)

// Slati searchText polje. Trazi threads sa odredenim title-om
router.post('/search',
    Thread.validate('searchThreads'),
    Thread.searchThreads
)

// Slati searchText polje. Trazi threads sa komentarima koji se sastoje od tog searchText-a.
router.post('/search_by_comments',
    Thread.validate('searchThreads'),
    Thread.searchThreadsByComments
)

// Dobavlja thread sa njezinim komentarima
router.get('/:id/comments',
    Thread.validate('getThread'),
    Thread.getWithComments
)

// Dobavlja thread
router.get('/:id',
    Thread.validate('getThread'),
    Thread.get
)

// Kreira komentar na odredeni thread
router.post('/:id',
    Comment.validate('createComment'),
    Comment.create
)

// Kreira thread
router.post('/',
    Thread.validate('createThread'),
    Thread.create
)

// Update threada
router.patch('/:id',
    Thread.validate('editThread'),
    Thread.edit
)

// Brisanje threada
router.delete('/:id',
    Thread.validate('deleteThread'),
    Thread.delete
)

module.exports = router