const express = require('express')
const router = express.Router()
const Comment = require('../database/Comment')

// Slati searchText polje, traži komentare sa određenim tekstom
router.post('/search',
    Comment.validate('searchComments'),
    Comment.searchComments
)

router.patch('/:id',
    Comment.validate('editComment'),
    Comment.edit
)

router.delete('/:id',
    Comment.validate('deleteComment'),
    Comment.delete
)

module.exports = router